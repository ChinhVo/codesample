// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EngineActivator.cs" company="">
//   
// </copyright>
// <summary>
//   The engine activator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System.Web;

using ILC.Mvc.App_Start;

[assembly: PreApplicationStartMethod(typeof(EngineActivator), "Start")]

namespace ILC.Mvc.App_Start
{
    using System.Configuration;
    using System.Reflection;
    using System.Web.Mvc;

    using Autofac;
    using Autofac.Integration.Mvc;

    using ILC.Business;
    using ILC.Component;
    using ILC.Core;
    using ILC.Core.Caching;
    using ILC.Core.Data.Infrastructure;
    using ILC.Core.Infrastructure.EngineContext;
    using ILC.Core.Security;
    using ILC.Data;
    using ILC.Data.Repositories;
    using ILC.Mvc.App_Start.Modules;
    using ILC.Services;
    using ILC.Services.Email;
    using ILC.Services.Templates;
    using ILC.Web.Core;
    using ILC.Web.Core.Security;
    
    /// <summary>
    /// The engine activator.
    /// </summary>
    public static class EngineActivator
    {
        #region Properties

        /// <summary>
        /// Gets or sets the _container buider.
        /// </summary>
        private static ContainerBuilder _containerBuider { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The start.
        /// </summary>
        public static void Start()
        {
            if (_containerBuider == null)
            {
                _containerBuider = new ContainerBuilder();
            }

            RegisterInstance(_containerBuider);
            EngineContext.Initialize(false, _containerBuider);
            DependencyResolver.SetResolver(
                new AutofacDependencyResolver(EngineContext.Current.ContainerManager.Container));
        }

        #endregion

        #region Methods

        /// <summary>
        /// The register instance.
        /// </summary>
        /// <param name="builder">
        /// The builder.
        /// </param>
        private static void RegisterInstance(ContainerBuilder builder)
        {
            

            builder.RegisterModule(new AutofacWebTypesModule());

            

            #region Register unitOfWork, repository

            string _ilcContextConnection = ConfigurationManager.ConnectionStrings["ILCDataContext"].ToString();

            builder.RegisterType<UnitOfWork<ILCDataContext>>()
                .As<IUnitOfWork<ILCDataContext>>()
                .WithParameter(new NamedParameter("connectionString", _ilcContextConnection))
                .InstancePerHttpRequest();

            builder.RegisterAssemblyTypes(typeof(ModuleRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerHttpRequest();

            #endregion

            #region Register BLLs

            builder.RegisterType<ILCUnitOfWork>().AsImplementedInterfaces().InstancePerHttpRequest();

            builder.RegisterAssemblyTypes(typeof(ControlBLL).Assembly)
                .Where(t => t.Name.EndsWith("BLL"))
                .AsImplementedInterfaces()
                .InstancePerHttpRequest();

            builder.RegisterType<DataBLL>().As<IDataBLL>().InstancePerHttpRequest();

            #endregion

            #region Register bussiness services

            builder.RegisterAssemblyTypes(typeof(CommonService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerHttpRequest();

            #endregion

            #region Register security
            
            builder.RegisterType<WebMembership>()
                .WithParameter(new NamedParameter("moduleName", AppGlobal.MODULE_NAME))
                .As<IWebMembership>()
                .InstancePerDependency();

            builder.RegisterType<WebSecurity>().As<IWebSecurity>().InstancePerDependency();

            #endregion

            #region Register worker

            builder.RegisterType<Worker>().As<IWorker>().InstancePerHttpRequest();

            #endregion

            #region Register controllers

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            #endregion

            #region Register email, template, logging, ...

            string exchangeServer = ConfigurationManager.AppSettings["ExchangeServer"];
            builder.RegisterType<EmailService>()
                .WithParameter(new NamedParameter("exchangeServerUrl", exchangeServer))
                .As<IEmailService>()
                .InstancePerHttpRequest();

            builder.RegisterType<TemplateService>().As<ITemplateService>().InstancePerHttpRequest();

            builder.RegisterModule(new LoggingModule());
            builder.RegisterType<Trace>().As<ITrace>().InstancePerHttpRequest();

            builder.RegisterType<MemoryCacheManager>()
                .As<ICacheManager>()
                .Named<ICacheManager>("ilc_cache_static")
                .SingleInstance();
            builder.RegisterType<PerRequestCacheManager>()
                .As<ICacheManager>()
                .Named<ICacheManager>("ilc_cache_per_request")
                .InstancePerHttpRequest();

            builder.RegisterType<WebHelper>().As<IWebHelper>().InstancePerHttpRequest();
            builder.RegisterType<ILC.Utilities.SystemFileStorage>().As<ILC.Utilities.IStorage>().InstancePerHttpRequest();

            #endregion

            #region Register others

            builder.RegisterType<ILC.Web.Core.Security.Encryptor>().As<IEncryptor>().InstancePerHttpRequest();
            builder.RegisterType<FunctionWorking>().AsImplementedInterfaces().InstancePerHttpRequest();

            #endregion
        }

        #endregion
    }
}

using Autofac;
using BPT.Domain;
using BPT.Repository;
using BPT.Services.Admin;
using BPT.Services.CTG;
using BPT.Services.HR;
using BPT.Services.HR.Travel;
using BPT.Services.HR.Travel.Ticket;
using BPT.Services.HR.Travel.Visa;
using System;
using System.Linq;

namespace BPT.Services.Configuration
{
    public class DependencyConfig
    {
        public static void Register(ContainerBuilder builder)
        {
            builder.RegisterType<BPTContext>().As<IBPTContext>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            
            builder.RegisterGeneric(typeof(Repository<>))
              .As(typeof(IRepository<>))
              .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(UserService).Assembly)
                .Where(t => t.Name.Contains("Service"))
                .AsImplementedInterfaces();
        }

    }
}

using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using BPT.Api.Controllers;

namespace BPT.Api
{
    public class DependencyConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            // Register all di from DAL
            Services.Configuration.DependencyConfig.Register(builder);

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}