using ILC.Utilities.CustomAttribute;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OSMA.Utilities.Generic
{
    public class ObjectHelper
    {
        public static List<Variance> CompareDetailed<Source, Destination>(Source source, Destination destination)
        {
            var variances = new List<Variance>();
            var sourceFields = source.GetType().GetProperties();
            var destinationFields = destination.GetType().GetProperties();
            foreach (var field in sourceFields)
            {
                if (Constants.ExceptionListField.Any(p => p == field.Name))
                {
                    continue;
                }
                var oldValue = field.GetValue(source, null);
                var fieldChange = destinationFields.FirstOrDefault(p => p.Name == field.Name);
                if (fieldChange == null)
                {
                    continue;
                }
                var descriptionAttibute = fieldChange.CustomAttributes.Where(p => p.AttributeType == typeof(DescriptionAttribute));
                var changeDescription = GetDescriptionAttr(fieldChange);
                if (string.IsNullOrEmpty(changeDescription))
                {
                    continue;
                }

                var newValue = fieldChange.GetValue(destination, null);
                if (oldValue == null && newValue != null || oldValue != null && !oldValue.Equals(newValue))
                {
                    var change = new Variance();
                    change.ID = Guid.NewGuid();
                    change.FieldName = changeDescription;
                    change.OldValue = FormatHistoryData(fieldChange, oldValue);
                    change.NewValue = FormatHistoryData(fieldChange, newValue);
                    change.FieldNameID = field.Name;
                    variances.Add(change);
                }
            }
            return variances;
        }

        private static object FormatHistoryData(PropertyInfo fieldChange, object inputValue)
        {
            if (inputValue == null)
            {
                return string.Empty;
            }
            var propertyType = fieldChange.PropertyType;
            if (fieldChange.PropertyType.IsGenericType && fieldChange.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                propertyType = Nullable.GetUnderlyingType(propertyType);
            }
            switch (Type.GetTypeCode(propertyType))
            {
                case TypeCode.Decimal:
                    return FormatCustomDecimal(inputValue, fieldChange);
                case TypeCode.Boolean:
                    return Convert.ToBoolean(inputValue) ? "Yes" : "No";
                case TypeCode.DateTime:
                    if (inputValue == null)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        var formatdate = Convert.ToDateTime(inputValue);
                        return formatdate.ToString(Constants.FormatDateDD_MMM_YYYY);
                    }
                default:
                    return inputValue;

            }
        }

        private static object FormatCustomDecimal(object inputValue, PropertyInfo fieldChange)
        {
            var result = inputValue;
            var formatValue = Convert.ToDecimal(inputValue);
            var isoCurrencyCode = Constants.FormatCurrency;
            var attributeType = fieldChange.CustomAttributes.LastOrDefault();
            if (attributeType == null)
            {
                return result;
            }
            else if (attributeType.AttributeType == typeof(FormatCurrencyAttribute))
            {
                return ToFormattedCurrencyString(formatValue, isoCurrencyCode, new CultureInfo(Constants.FormatCultureDecimal, false));
            }
            else if (attributeType.AttributeType == typeof(FormatDecimalAttribute))
            {
                return string.Format(new CultureInfo(Constants.FormatCultureDecimal, false), Constants.FormatDecimalPoint, formatValue);
            }
            else
            {
                return result;
            }
        }

        public static byte[] ConvertToByteWithString(string[] input)
        {
            byte[] bytes = new byte[input.Length];
            for (int index = 0; index < bytes.Length; index++)
            {
                byte current = Convert.ToByte(input[index]);
                bytes[index] = current;
            }
            return bytes;
        }

        public static string[] CreateArrayStringFromByteArray(byte[] byteArray)
        {
            var result = new string[byteArray.Length];

            for (var index = 0; index < byteArray.Length; index++)
            {
                result[index] = byteArray[index].ToString();
            }
            return result;
        }

        public static string Serialize<T>(T dataToSerialize)
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(stringwriter, dataToSerialize);
            return stringwriter.ToString();
        }

        public static T Deserialize<T>(string xmlText) where T : new()
        {
            if (string.IsNullOrEmpty(xmlText))
                return new T();
            var stringReader = new System.IO.StringReader(xmlText);
            var serializer = new XmlSerializer(typeof(T));
            return (T)serializer.Deserialize(stringReader);
        }

        /// <summary>
        /// GetDescriptionAttr
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string GetDescriptionAttr(PropertyInfo source)
        {
            var descriptionAttibute = Attribute.GetCustomAttribute(source, typeof(DescriptionAttribute)) as DescriptionAttribute;

            if (descriptionAttibute == null)
            {
                return string.Empty;
            }
            else
            {
                return descriptionAttibute.Description;
            }
        }

        public static string ToFormattedCurrencyString(decimal currencyAmount, string isoCurrencyCode, CultureInfo userCulture)
        {
            return string.Format(
                "{0} {1}",
                 currencyAmount.ToString("N2", userCulture),
                 isoCurrencyCode);
        }
        
        public static string FormatCurrency(this decimal amount, string currencyCode)
        {
            var culture = (from c in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                           let r = new RegionInfo(c.LCID)
                           where r != null
                           && r.ISOCurrencySymbol.ToUpper() == currencyCode.ToUpper()
                           select c).FirstOrDefault();

            if (culture == null)
                return amount.ToString("0.00");

            return string.Format(culture, "{0:C}", amount);
        }
        
                public static string GetBaseUrl()
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            if (appUrl.IndexOf('/') < 0)
            {
                appUrl = "/" + appUrl;
            }

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }

        public static string GetSubdomain()
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;     
            return appUrl;
        }
        
        private List<int> GetChildByCatId(List<int> parentCatIds)
        {
            var childCatIds = riskCategoryRepository.GetMany(cat => cat.ParentId.HasValue && parentCatIds.Contains(cat.ParentId.Value))
              .Select(cat => cat.Id)
              .ToList();
            if (childCatIds.Count == 0)
                return parentCatIds;
            else
                return GetChildByCatId(childCatIds);
        }
        
        
  private static object FormatEnumDescription(object inputValue, PropertyInfo fieldChange)
  {
      var result = inputValue;
      var formatValue = Convert.ToInt32(inputValue);
      var attributeType = fieldChange.CustomAttributes.Where(p=>p.AttributeType == typeof(FormatEmumDescriptionAttribute));
      if (attributeType!=null)
      {
          return GetEnumDescription<gnt>(formatValue);
      }           
      else
      {
          return result;
      }
  }
  
      [AttributeUsage(AttributeTargets.Property)]
    public class FormatEmumDescriptionAttribute : Attribute, IMetadataAware
    {

        public bool AllowMultiple
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {

        }

    }
    }
}
