CREATE FUNCTION dbo.fnYearsApart
(
        @FromDate DATETIME,
        @ToDate DATETIME
)
RETURNS INT
AS
BEGIN
        RETURN  CASE
                       WHEN @FromDate > @ToDate THEN NULL
                       WHEN DATEPART(day, @FromDate) > DATEPART(day, @ToDate) THEN DATEDIFF(month, @FromDate, @ToDate) - 1
                       ELSE DATEDIFF(month, @FromDate, @ToDate)
               END / 12
END

CREATE FUNCTION dbo.fnMonthsApart
(
        @FromDate DATETIME,
        @ToDate DATETIME
)
RETURNS INT
AS
BEGIN
        RETURN  CASE
                       WHEN @FromDate > @ToDate THEN NULL
                       WHEN DATEPART(day, @FromDate) > DATEPART(day, @ToDate) THEN DATEDIFF(month, @FromDate, @ToDate) - 1
                       ELSE DATEDIFF(month, @FromDate, @ToDate)
               END
END


Alter FUNCTION [dbo].[gatYMD](@dstart VARCHAR(50), @dend VARCHAR(50))
RETURNS VARCHAR(50) AS
BEGIN
    DECLARE @yy INT
    DECLARE @mm INT
    DECLARE @dd INT
    DECLARE @getmm INT
    DECLARE @getdd INT

    SET @yy = dbo.fnYearsApart(@dstart, @dend)  --DATEDIFF(yy, @dstart, @dend)
    SET @mm = dbo.fnMonthsApart(@dstart, @dend) --DATEDIFF(mm, @dstart, @dend)

    SET @dd = DATEDIFF(dd, @dstart, @dend)
    SET @getmm = ABS(DATEDIFF(mm, DATEADD(yy, @yy, @dstart), @dend))
    SET @getdd = ABS(DATEDIFF(dd, DATEADD(mm, DATEDIFF(mm, DATEADD(yy, @yy, @dstart), @dend), DATEADD(yy, @yy, @dstart)), @dend))
	if(@getdd <>0 )
		BEGIN
			RETURN (
			  Convert(varchar(10),@mm) + ' tháng '  +  Convert(varchar(10),@getdd) + ' ngày'
			)
		end	 
	ELSE
		BEGIN
	  		RETURN (
			  Convert(varchar(10),@mm) + ' tháng '
			)
		end
	RETURN ''
END

SELECT [dbo].[gatYMD]('2017-08-25 00:00:00.000', '2018-08-25 00:00:00.000')


ALTER FUNCTION dbo.GetDateDifferenceInYearsMonthsDays
(
    @FromDate DATETIME, @ToDate DATETIME
)
RETURNS
 @DateDifference TABLE (
 YEAR INT,  MONTH INT, DAYS INT)
AS
BEGIN
    DECLARE @Years INT, @Months INT, @Days INT, @tmpFromDate DATETIME
    SET @Years = DATEDIFF(YEAR, @FromDate, @ToDate)
     - (CASE WHEN DATEADD(YEAR, DATEDIFF(YEAR, @FromDate, @ToDate),
              @FromDate) > @ToDate THEN 1 ELSE 0 END) 
    
    SET @tmpFromDate = DATEADD(YEAR, @Years , @FromDate)
    SET @Months =  DATEDIFF(MONTH, @tmpFromDate, @ToDate) - (CASE WHEN DATEADD(MONTH,DATEDIFF(MONTH, @tmpFromDate, @ToDate), @tmpFromDate) > @ToDate THEN 1 ELSE 0 END) 
    
    SET @tmpFromDate = DATEADD(MONTH, @Months , @tmpFromDate)

    SET @Days =  DATEDIFF(DAY, @tmpFromDate, @ToDate) - (CASE WHEN DATEADD(DAY, DATEDIFF(DAY, @tmpFromDate, @ToDate), @tmpFromDate) > @ToDate THEN 1 ELSE 0 END) 
    
    INSERT INTO @DateDifference
    VALUES(@Years, @Months, @Days)
    
    RETURN
END


ALTER FUNCTION dbo.GetDateDifferenceInYearsMonthsDays
(
    @FromDate DATETIME, @ToDate DATETIME
)
RETURNS
 @DateDifference TABLE (
 YEAR INT,  MONTH INT, DAYS INT)
AS
BEGIN
    DECLARE @Years INT, @Months INT, @Days INT, @tmpFromDate DATETIME
    SET @Years = DATEDIFF(YEAR, @FromDate, @ToDate)
     - (CASE WHEN DATEADD(YEAR, DATEDIFF(YEAR, @FromDate, @ToDate),
              @FromDate) > @ToDate THEN 1 ELSE 0 END) 
    
    SET @tmpFromDate = DATEADD(YEAR, @Years , @FromDate)
    SET @Months =  DATEDIFF(MONTH, @tmpFromDate, @ToDate) - (CASE WHEN DATEADD(MONTH,DATEDIFF(MONTH, @tmpFromDate, @ToDate), @tmpFromDate) > @ToDate THEN 1 ELSE 0 END) 
    
    SET @tmpFromDate = DATEADD(MONTH, @Months , @tmpFromDate)

    SET @Days =  DATEDIFF(DAY, @tmpFromDate, @ToDate) - (CASE WHEN DATEADD(DAY, DATEDIFF(DAY, @tmpFromDate, @ToDate), @tmpFromDate) > @ToDate THEN 1 ELSE 0 END) 
    
    INSERT INTO @DateDifference
    VALUES(@Years, @Months, @Days)
    
    RETURN
END

---Get quater start-end date
DECLARE @Year DATE = DATEADD(DD,-DATEPART(DY,GETDATE())+1,GETDATE())

DECLARE @Quarter INT = 4;

SELECT  DATEADD(QUARTER, @Quarter - 1, @Year) ,
        DATEADD(DAY, -1, DATEADD(QUARTER,  @Quarter, @Year))
        
--Get start - end quater date
DECLARE @AnyDate DATETIME
SET @AnyDate = GETDATE()
 
SELECT @AnyDate AS 'Input Date',
  DATEADD(q, DATEDIFF(q, 0, @AnyDate), 0) 
                        AS 'Quarter Start Date',       
  DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, @AnyDate) + 1, 0)) 
                        AS 'Quarter End Date'
                        
--next quater
DECLARE @AnyDate DATETIME
SET @AnyDate = GETDATE()
 
SELECT @AnyDate AS 'Input Date',
    DATEADD(q, DATEDIFF(q, 0, @AnyDate), 0) 
                               AS 'Quarter Start Date',       
    DATEADD(q, DATEDIFF(q, 0, @AnyDate) + 1, 0) 
                               AS 'Next Quarter Start Date'
                              
--quater by day
SELECT DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0)

SELECT DATEADD(dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0))
 
--!View:
SELECT c.name AS ColName, t.name AS TableName
FROM sys.columns c
    JOIN sys.tables t ON c.object_id = t.object_id
WHERE c.name LIKE '%IsProcessed%'
        
----------------------------------
--View:
SELECT COLUMN_NAME, TABLE_NAME 
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE COLUMN_NAME LIKE '%POI_SICode%'
---------------------------------------------------
DECLARE @Search varchar(255) ='hrm_Employees'
SELECT DISTINCT
    o.name AS Object_Name,o.type_desc
    FROM sys.sql_modules m 
        INNER JOIN sys.objects o ON m.object_id=o.object_id
    WHERE m.definition Like '%'+@Search+'%'-- AND m.definition Like '%insert%'
    ORDER BY 2,1 


---COALESCE
DECLARE @ExpatRoleEmail NVARCHAR(max) ;
 WITH 
  ExpatRoleEmail (Email)
  AS
  (
	  SELECT DISTINCT E.Email
	  FROM  T_COM_Master_Employee E			
	  JOIN T_COM_Master_RoleUserMapping roleuser on E.EmployeeCode = roleuser.EmployeeCode 							
	  WHERE roleuser.RoleID = @ExpatRole	 and E.Email is not NULL and E.IsActive = 1
  )

SELECT @ExpatRoleEmail = COALESCE(@ExpatRoleEmail + '; ', '') + Email  FROM   ExpatRoleEmail

---using STUFF
SELECT
     [WIT Code],GBID,
     STUFF(
         (SELECT DISTINCT ',' + CONVERT(NVARCHAR(MAX), [WHFunctionID]
          FROM  [WIT_App].[dbo].['multi WH function$']
          WHERE GBID = a.GBID
          FOR XML PATH (''))
          , 1, 1, '')  AS URLList
FROM  [WIT_App].[dbo].['multi WH function$'] AS a
GROUP BY [WIT Code], GBID

----Delete duplicate row
with CTE_R as
(
    select
        *,
        ROW_NUMBER() OVER(PARTITION BY Name ORDER BY Name) as RowNum
    from employee
)

DELETE FROM CTE_R
WHERE RowNum > 1

USE [StoreManagement]
GO
/****** Object:  UserDefinedFunction [dbo].[TRIM]    Script Date: 11/5/2017 5:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Function
ALTER FUNCTION [dbo].[TRIM](@string nVARCHAR(MAX))
RETURNS nVARCHAR(MAX)
BEGIN
RETURN LTRIM(RTRIM(@string))
END


  update [StoreManagement].[dbo].[Sheet1$]
    set [TÊN XE] = rtrim(ltrim(case when [TÊN XE] not like '[a-zA-Z0-9]%'
                                then stuff([TÊN XE], 1, 1, '')
                                else [TÊN XE]
                           end)
                    );


/**Dump path parent child**/
USE [ILC_R]
GO
/****** Object:  Trigger [dbo].[TRG_After_Insert_Risk_Category]    Script Date: 11/22/2017 6:26:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chinh Vo
-- Create date: AND
-- Description:	TRG_After_Insert_Risk_Category
-- =============================================
ALTER TRIGGER [dbo].[TRG_After_Insert_Risk_Category] 
ON [dbo].[RiskCategory]
AFTER INSERT AS
BEGIN	 
	UPDATE 	[dbo].[RiskCategory]
	SET Path = cast(convert(nvarchar(255), Id) as NVARCHAR(255))
	WHERE Id = (SELECT ID from INSERTED)
END

USE [ILC_R]
GO
/****** Object:  Trigger [dbo].[TRG_After_Update_Risk_Category]    Script Date: 11/22/2017 6:27:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chinh Vo
-- Create date: AND
-- Description:	TRG_After_Update_Risk_Category
-- =============================================
ALTER TRIGGER [dbo].[TRG_After_Update_Risk_Category] 
ON [dbo].[RiskCategory]
AFTER UPDATE AS
BEGIN
	WITH group_paths (group_id, group_path)
	AS
	(
		SELECT g.ID, cast(convert(nvarchar(255), g.Id) as NVARCHAR(255)) as group_path
		FROM [ILC_R].[dbo].[RiskCategory] AS g
		WHERE g.ParentId is NULL
		UNION ALL

		SELECT g.Id, cast(group_path + '/' + convert(nvarchar(255), g.Id)as NVARCHAR(255)) as 	  group_path
		FROM [ILC_R].[dbo].[RiskCategory] AS g
		INNER JOIN group_paths AS gp
		ON g.ParentId = gp.group_id
	)

	--SELECT group_id, group_path
	--FROM group_paths

	UPDATE 	[dbo].[RiskCategory]
	SET Path = group_path
	FROM 	 [dbo].[RiskCategory]  as category
	JOIN 	 group_paths as pth on pth.group_id = category.Id 
END


CREATE FUNCTION FindDuplicateLetters
(
    @String NVARCHAR(50)
)
RETURNS BIT
AS
BEGIN

    DECLARE @Result BIT = 0 
    DECLARE @Counter INT = 1

    WHILE (@Counter <= LEN(@String) - 1) 
    BEGIN


    IF(ASCII((SELECT SUBSTRING(@String, @Counter, 1))) = ASCII((SELECT SUBSTRING(@String, @Counter + 1, 1))))
        BEGIN
             SET @Result = 1
             BREAK
        END


        SET @Counter = @Counter + 1 
    END

    RETURN @Result

END
GO

----
SELECT 
    * 
FROM
    (SELECT 
        *, 
        dbo.FindDuplicateLetters(Leave) AS Duplicates
    FROM T_TMS_EmployeeTimesheetWeeklyDetails) AS a
WHERE a.Duplicates = 1
