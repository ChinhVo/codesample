using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace OSMA.Security
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AjaxValidateAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            try
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest()) 
                {
                    this.ValidateRequestHeader(filterContext.HttpContext.Request);
                }
                else
                {
                    AntiForgery.Validate();
                }
            }
            catch (HttpAntiForgeryException e)
            {
                throw new HttpAntiForgeryException("Anti forgery token not found");
            }
        }

        private void ValidateRequestHeader(HttpRequestBase request)
        {
            string cookieToken = string.Empty;
            string formToken = string.Empty;
            string tokenValue = request.Headers["VerificationToken"]; 
            if (!string.IsNullOrEmpty(tokenValue))
            {
                string[] tokens = tokenValue.Split(',');
                if (tokens.Length == 2)
                {
                    cookieToken = tokens[0].Trim();
                    formToken = tokens[1].Trim();
                }
            }

            AntiForgery.Validate(cookieToken, formToken);
        }
    }
}

<script type="text/javascript">
        $(document).ready(function () {
            updateActiveMenu();
        });
         @functions{
            public string GetAntiForgeryToken()
            {
                string cookieToken, formToken;
                AntiForgery.GetTokens(null, out cookieToken, out formToken);
                return cookieToken + "," + formToken;
            }
        }
    </script>
<input type="hidden" id="forgeryToken" value="@GetAntiForgeryToken()" />

var forgeryId = $("#forgeryToken").val();

headers: {
 'VerificationToken': forgeryId
},

public void OnAuthorization(AuthorizationContext filterContext)
        {
            var request = filterContext.HttpContext?.Request;
            if (request != null && "application/json".Equals(request.ContentType, StringComparison.OrdinalIgnoreCase))
            {
                if (request.ContentLength > 0 && request.Form.Count == 0) // 
                {
                    if (request.InputStream.Position > 0)
                        request.InputStream.Position = 0; 
                    using (var reader = new StreamReader(request.InputStream))
                    {
                        var postedContent = reader.ReadToEnd();
                        int failureIndex;
                        var isValid = RequestValidator.Current.InvokeIsValidRequestString(
                            HttpContext.Current, postedContent,
                            RequestValidationSource.Form, "postedJson", out failureIndex); 
                        if (!isValid)
                        {
                            throw new HttpRequestValidationException("Potentially unsafe input detected");
                        }
                    }
                }
            }
        }
        
https://www.owasp.org/index.php/ASP.NET_Request_Validation
http://www.c-sharpcorner.com/article/avoid-cross-site-scripting-xss-attacks-while-posting-data-through-ajax-in-mvc/