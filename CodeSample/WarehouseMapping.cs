namespace OSMA.Services.Models
{
    using System.Linq;

    using AutoMapper;

    using OSMA.Component;
    using OSMA.Data;
    using OSMA.Core.Mapper.Extensions;
    using OSMA.Core.Mapper;
    using OSMA.Services.Audit.Model;
    using OSMA.Services.Configuration.Task;
    using OSMA.Services.Enum.Task;
    using OSMA.Services.Helper;
    using Warehouse.Models;

    /// <summary>
    /// The province mapping.
    /// </summary>
    public class WarehouseMapping : EntityMappingBase<Warehouse, WarehouseModel>
    {
        #region Methods

        /// <summary>
        /// The configure mapping.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="reverseMapper">
        /// The reverse mapper.
        /// </param>
        protected override void ConfigureMapping(
            IMappingExpression<Warehouse, WarehouseModel> mapper,
            IMappingExpression<WarehouseModel, Warehouse> reverseMapper)
        {

        }

        #endregion
    }
  
}

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntityMappingBase.cs" company="">
//   
// </copyright>
// <summary>
//   Base class that auto export Profile for config AutoMapper.
//   Implement this class by specify Entity type and DTO type that need to map.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace OSMA.Services.Mapping
{
    using AutoMapper;

    /// <summary>
    /// Base class that auto export Profile for config AutoMapper.
    ///     Implement this class by specify Entity type and DTO type that need to map.
    /// </summary>
    /// <typeparam name="TEntity">
    /// Entity type to map.
    /// </typeparam>
    /// <typeparam name="TDto">
    /// DTO type that map to.
    /// </typeparam>
    public abstract class EntityMappingBase<TEntity, TDto> : Profile
    {
        #region Methods

        /// <summary>
        ///     Configure mapping.
        /// </summary>
        protected override void Configure()
        {
            this.ConfigureMapping(this.CreateMap<TEntity, TDto>(), this.CreateMap<TDto, TEntity>());
        }

        /// <summary>
        /// The configure mapping.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="reverseMapper">
        /// The reverse Mapper.
        /// </param>
        protected abstract void ConfigureMapping(
            IMappingExpression<TEntity, TDto> mapper, 
            IMappingExpression<TDto, TEntity> reverseMapper);

        #endregion
    }
}

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MapperConfig.cs" company="">
//   
// </copyright>
// <summary>
//   The mapper config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WebApp.Mvc.App_Start
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Autofac;

    using AutoMapper;

    using ILC.Core.Mapper;
    using ILC.Services.Control;

    /// <summary>
    /// The mapper config.
    /// </summary>
    public static class MapperConfig
    {
        #region Public Methods and Operators

        /// <summary>
        /// The initialize.
        /// </summary>
        public static void Initialize()
        {
            Mapper.Initialize(GetConfiguration);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get all profiles and add to configuration.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        private static void GetConfiguration(IConfiguration configuration)
        {
            Assembly mappingAssembly = typeof(ControlMapping).Assembly;
            IEnumerable<Type> typesToRegister =
                mappingAssembly.GetTypes()
                    .Where(type => !string.IsNullOrEmpty(type.Namespace))
                    .Where(
                        type =>
                        type.BaseType != null && type.BaseType.IsGenericType
                        && type.BaseType.GetGenericTypeDefinition() == typeof(EntityMappingBase<,>));

            foreach (Type type in typesToRegister)
            {
                var profile = Activator.CreateInstance(type) as Profile;

                if (profile != null)
                {
                    configuration.AddProfile(profile);

                    if (!profile.GetType().IsClosedTypeOf(typeof(EntityMappingBase<,>)))
                    {
                        continue;
                    }

                    Type baseType = profile.GetType().BaseType;
                    if (baseType == null)
                    {
                    }
                }
            }
        }

        #endregion
    }
}