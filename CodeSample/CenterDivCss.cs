You can apply this CSS to the inner <div>:

#inner {
  width: 50%;
  margin: 0 auto;
}
Of course, you don't have to set the width to 50%. Any width less than the containing <div> will work. The margin: 0 auto is what does the actual centering.

If you are targeting IE8+, it might be better to have this instead:

#inner {
  display: table;
  margin: 0 auto;
}
It will make the inner element center horizontally and it works without setting a specific width.

Working example here:

#inner {
  display: table;
  margin: 0 auto;
}
<div id="outer" style="width:100%">
  <div id="inner">Foo foo</div>
</div>

.navbar .navbar-nav {
    display: inline-block;
    float: none;
    margin-bottom: -3px;
}

.main-header {
    z-index: 1030;
    width: 100%;
    border-image: url(./images/navTopBar.png) 10% 10% 10% 10%;
    border-style: solid none none;
    border-width: 10px 0px 0px;
    background-color: white;
    height: auto;
    max-height: inherit;
    position: fixed;
    top: 0;
}

var options = $('#selectBox option');
var values = $.map(options ,function(option) {
    return option.value;
});


/*fix header css and footer*/
.main-header {
    z-index: 1030;
    width: 100%;
    border-image: url(./images/navTopBar.png) 10% 10% 10% 10%;
    border-style: solid none none;
    border-width: 10px 0 0;
    background-color: #fff;
    height: auto;
    max-height: inherit;
    position: fixed;
    top: 0;
}

.main-footer {
    bottom: 0;
    position: fixed;
    width: 100%;
    font-size: 16px;
    padding: 10px;
}

.main-footer {
    background: #fff;
    padding: 15px;
    color: #444;
    border-top: 1px solid #d2d6de;
}

footer {
    height: 45px;
    padding: 10px;
}

.content-wrapper, .right-side, .main-footer {
    -webkit-transition: -webkit-transform 0.3s ease-in-out, margin 0.3s ease-in-out;
    -moz-transition: -moz-transform 0.3s ease-in-out, margin 0.3s ease-in-out;
    -o-transition: -o-transform 0.3s ease-in-out, margin 0.3s ease-in-out;
    transition: transform 0.3s ease-in-out, margin 0.3s ease-in-out;
    z-index: 820;
}

/*scroll to top*/

<a href="#" class="scrollup" title="Go up page"><i class="fa fa-angle-up active"></i></a>
/* scroll to top */
.scrollup {
    position: fixed;
    width: 32px;
    height: 32px;
    bottom: 35px;
    right: 25px;
    background: #222;
    z-index: 950;
    display: none;
}

a.scrollup {
    outline: 0;
    text-align: center;
}

    a.scrollup:hover, a.scrollup:active, a.scrollup:focus {
        opacity: 1;
        text-decoration: none;
    }

    a.scrollup i {
        margin-top: 10px;
        color: #fff;
    }

        a.scrollup i:hover {
            text-decoration: none;
        }
/*end scroll to top */
    $(window).scroll(function () {
        var bias = 50;
        if ($(this).scrollTop() > bias) {
            $('.scrollup').show();
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
    $('.scrollup').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 800);
        return false;
    });

function updateActiveMenu() {
    var url = document.URL.toLocaleLowerCase();
    var listMenu = $("#navbar-collapse-2 li");
    listMenu.each(function (item) {
        $(this).removeClass("active-menu");
    });
    $("#admin-menu").removeClass("active-menu");
    listMenu.each(function (item) {
        var hrefLink = $(this).find("a").attr("href").replace("/", "").toLocaleLowerCase();
        if (url.indexOf(hrefLink) > 0) {
            $(this).addClass("active-menu");
            if ($("#admin-menu").find(this).length > 0) {
                $("#admin-menu").addClass("active-menu");
            }
        }

    });
    if (location.pathname == '/' || !location.pathname || location.pathname.replaceAll('/','') == subdomain.replaceAll('/','')) {
        $("#default-active-menu").addClass("active-menu");
    }

}

/*Get by atrribute*/
var identifyKey = $tr.find('input[identifykey]').attr("identifykey");
var identifyKey = $(this).attr('IdentifyKey');


function triggerScroll() {
    var previousScroll = 200;
    $(window).scroll(function (event) {
        var scroll = $(this).scrollTop();
        if (scroll > previousScroll) {
            $(".page-navigation").addClass('over-when-scroll');
        } else {
            $(".page-navigation").removeClass('over-when-scroll');
        }
        previousScroll = scroll;
    });
}

.over-when-scroll {
    position: fixed;
    top: 50%;
    left: 50%;
    top: 35px;
    left: 50%;
    width: 100%;
    transform: translate(-50%, -50%);
}