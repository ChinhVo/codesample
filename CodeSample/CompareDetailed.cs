﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeSample
{
    public class Class1
    {
         public static List<Variance> CompareDetailed<Source, Destination>(Source source, Destination destination)
        {
            var variances = new List<Variance>();
            var sourceFields = source.GetType().GetProperties();
            var destinationFields = destination.GetType().GetProperties();
            foreach (var field in sourceFields)
            {
                if (Constants.ExceptionListField.Any(p => p == field.Name))
                {
                    continue;
                }
                var change = new Variance();
                change.OldValue = field.GetValue(source, null);
                var fieldChange = destinationFields.FirstOrDefault(p => p.Name == field.Name);
                if (fieldChange == null)
                {
                    continue;
                }
                var descriptionAttibute = fieldChange.CustomAttributes.Where(p => p.AttributeType == typeof(DescriptionAttribute));
                change.FieldName = GetDescriptionAttr(fieldChange);

                change.NewValue = fieldChange.GetValue(destination, null);
                if (change.OldValue == null && change.NewValue != null || change.OldValue != null && !change.OldValue.Equals(change.NewValue))
                {
                    change.ID = Guid.NewGuid();
                    variances.Add(change);
                }
            }
            return variances;
        }

        public static byte[] ConvertToByteWithString(string[] input)
        {
            byte[] bytes = new byte[input.Length];
            for (int index = 0; index < bytes.Length; index++)
            {
                byte current = Convert.ToByte(input[index]);
                bytes[index] = current;
            }
            return bytes;
        }

        public static string[] CreateArrayStringFromByteArray(byte[] byteArray)
        {
            var result = new string[byteArray.Length];

            for (var index = 0; index < byteArray.Length; index++)
            {
                result[index] = byteArray[index].ToString();
            }
            return result;
        }
    
        /// <summary>
        /// GetDescriptionAttr
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string GetDescriptionAttr(PropertyInfo source)
        {
            var descriptionAttibute = Attribute.GetCustomAttribute(source, typeof(DescriptionAttribute)) as DescriptionAttribute;

            if (descriptionAttibute == null)
            {
                return source.Name;
            }
            else
            {
                return descriptionAttibute.Description;
            }
        }
    }
}
