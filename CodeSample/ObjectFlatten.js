var flattenObject = function(ob) {
	var toReturn = {};
	
	for (var i in ob) {
		if (!ob.hasOwnProperty(i)) continue;
		
		if ((typeof ob[i]) == 'object') {
			var flatObject = flattenObject(ob[i]);
			for (var x in flatObject) {
				if (!flatObject.hasOwnProperty(x)) continue;
				
				toReturn[i + '.' + x] = flatObject[x];
			}
		} else {
			toReturn[i] = ob[i];
		}
	}
	return toReturn;
};

flattenObject = (ob) ->
  what = Object.prototype.toString
  toReturn = {}
  for i of ob
    if !ob.hasOwnProperty(i)
      continue
    result = what.call(ob[i])
    if result == '[object Object]' or result == '[object Array]'
      flatObject = flattenObject(ob[i])
      for x of flatObject
        if !flatObject.hasOwnProperty(x)
          continue
        toReturn[i + '.' + x] = flatObject[x]
    else
      toReturn[i] = ob[i]
  toReturn
  
  var flattenObject = function(ob) {
  
  return Object.keys(ob).reduce(function(toReturn, k) {

    if (Object.prototype.toString.call(ob[k]) === '[object Date]') {
      toReturn[k] = ob[k].toString();
    }
    else if ((typeof ob[k]) === 'object' && ob[k]) {
      var flatObject = flattenObject(ob[k]);
      Object.keys(flatObject).forEach(function(k2) {
        toReturn[k + '.' + k2] = flatObject[k2];
      });
    }
    else {
      toReturn[k] = ob[k];
    }

    return toReturn;
  }, {});
};

I created this as an ES6 closure:

/**
 * PRIVATE
 * Flatten a deep object into a one level object with it’s path as key
 *
 * @param  {object} object - The object to be flattened
 *
 * @return {object}        - The resulting flat object
 */
const flatten = object => {
  return Object.assign( {}, ...function _flatten( objectBit, path = '' ) {  //spread the result into our return object
    return [].concat(                                                       //concat everything into one level
      ...Object.keys( objectBit ).map(                                      //iterate over object
        key => typeof objectBit[ key ] === 'object' ?                       //check if there is a nested object
          _flatten( objectBit[ key ], `${ path }/${ key }` ) :              //call itself if there is
          ( { [ `${ path }/${ key }` ]: objectBit[ key ] } )                //append object with it’s path as key
      )
    )
  }( object ) );
};

Call it via:

const flat = flatten( realDeepObject );

Test case:

const realDeepObject = {
  level1: {
    level2: {
      level3: {
        more: 'stuff', //duplicate key
        other: 'stuff',
        level4: {
          the: 'end',
        },
      },
    },
    level2still: {
      last: 'one',
    },
    am: 'bored',
  },
  more: 'stuff', //duplicate key
  ipsum: {
    lorem: 'latin',
  },
};

const flat = flatten( realDeepObject );

console.log( flat );

private flattenObject(ob:any): any {
        const toReturn = {};

        for (const key in ob) {
            if (!ob.hasOwnProperty(key)) {
                continue;
            }
            if ((typeof ob[key]) === 'object') {
                const flatObject = this.flattenObject(ob[key]);
                for (const key2 in flatObject) {
                    if (!flatObject.hasOwnProperty(key2)) {
                        continue;
                    }
                    // this.logger.debug(`adding ${key + '.' + key2}:${flatObject[key2]}`);
                    toReturn[key + '.' + key2] = flatObject[key2];
                }
            } else {
                // this.logger.debug(`adding ${key}:${ob[key]}`);
                toReturn[key] = ob[key];
            }
        }
        return toReturn;
    };
    
function flatten(object, separator = '.') {
    return Object.assign({}, ...function _flatten(child, path = []) {
        return [].concat(...Object.keys(child).map(key => typeof child[key] === 'object'
            ? _flatten(child[key], path.concat([key]))
            : ({ [path.concat([key]).join(separator)] : child[key] })
        ));
    }(object));
}

function flatten(object: object, separator = '.'): object {
  const isValidObject = (value): boolean => {
    if (!value) { return false; }
    const isArray = Array.isArray(value);
    const isBuffer = Buffer.isBuffer(value);
    const isΟbject = Object.prototype.toString.call(value) === "[object Object]";
    const hasKeys = !!Object.keys(value).length;
    return !isArray && !isBuffer && isΟbject && hasKeys;
  };
  return Object.assign({}, ...function _flatten(child, path = []) {
    return [].concat(...Object.keys(child)
      .map(key => isValidObject(child[key])
        ? _flatten(child[key], path.concat([key]))
        : { [path.concat([key]).join(separator)]: child[key] }));
  }(object));
}

function flatten(object, separator = '.') {
	const isValidObject = value => {
		if (!value) {
			return false
		}

		const isArray  = Array.isArray(value)
		const isObject = Object.prototype.toString.call(value) === '[object Object]'
		const hasKeys  = !!Object.keys(value).length

		return !isArray && isObject && hasKeys
	}

	const walker = (child, path = []) => {

		return Object.assign({}, ...Object.keys(child).map(key => isValidObject(child[key])
			? walker(child[key], path.concat([key]))
			: { [path.concat([key]).join(separator)] : child[key] })
		)
	}

	return Object.assign({}, walker(object))
}

function flatten(arr) {
  return arr.reduce(function(explored, toExplore) {
    return explored.concat(
      Array.isArray(toExplore)
        ? flatten(toExplore)
        : toExplore
    );
  }, []);
}