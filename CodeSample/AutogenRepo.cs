<#@ template language="C#" debug="false" hostspecific="true"#>
<#@ include file="EF.Utility.CS.ttinclude"#>
<#@ output extension=".cs"#>

//*********************************************************
//
//    Copyright (c) Microsoft. All rights reserved.
//    This code is licensed under the Microsoft Public License.
//    THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
//    ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
//    IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
//    PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

<#
CodeGenerationTools code = new CodeGenerationTools(this);

MetadataLoader loader = new MetadataLoader(this);

CodeRegion region = new CodeRegion(this, 1);

MetadataTools ef = new MetadataTools(this);

//*********************************************************
//    The edmx file path
//
//*********************************************************
string edmxFilePath = @"..\ILCDataContext.edmx";

string namespaceName = code.VsNamespaceSuggestion();

EdmItemCollection ItemCollection = loader.CreateEdmItemCollection(edmxFilePath);

EntityContainer container = ItemCollection.GetItems<EntityContainer>().FirstOrDefault();
if (container == null)
{
    return string.Empty;
}

EntityFrameworkTemplateFileManager fileManager = EntityFrameworkTemplateFileManager.Create(this);

// write header file
WriteHeader(fileManager);

foreach (var entity in ItemCollection.GetItems<EntityType>().OrderBy(e => e.Name))
{
    fileManager.StartNewFile(entity.Name + "Repository.cs");
    BeginNamespace(namespaceName, code, entity);
#>
/// <summary>
/// The <#=code.Escape(entity)#>Repository interface.
/// </summary>
public interface I<#=code.Escape(entity)#>Repository : IRepository<<#=code.Escape(entity)#>>
{
}

/// <summary>
/// The <#=code.Escape(entity)#> repository.
/// </summary>
<#=Accessibility.ForType(entity)#> <#=code.SpaceAfter(code.AbstractOption(entity))#>partial class <#=code.Escape(entity)#>Repository<#=code.StringBefore(" : ", "RepositoryBase<" + code.Escape(container) + ", "  +code.Escape(entity) + ">, I" + code.Escape(entity)+ "Repository" )#>
{    
    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="<#=code.Escape(entity)#>Repository"/> class.
    /// </summary>
    /// <param name="unitOfWork">
    /// The unit of work.
    /// </param>
    public <#=code.Escape(entity)#>Repository(IUnitOfWork<<#= code.Escape(container) #>> unitOfWork) 
        : base(unitOfWork.DataContext)
    {
    }    

    #endregion
}
<#
    EndNamespace(namespaceName);
}
fileManager.Process();
#>

<#+
void WriteHeader(EntityFrameworkTemplateFileManager fileManager)
{
    fileManager.StartHeader();
#>
<#+
    fileManager.EndBlock();
}
void BeginNamespace(string namespaceName, CodeGenerationTools code, EntityType entity)
{
    CodeRegion region = new CodeRegion(this);
    if (!String.IsNullOrEmpty(namespaceName))
    {
#>
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="<#=code.Escape(entity)#>Repository.cs" company="">
//   
// </copyright>
// <summary>
// The <#=code.Escape(entity)#> repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace <#=code.EscapeNamespace(namespaceName)#>
{
    using ILC.Core.Data.Infrastructure;

<#+
        PushIndent(CodeRegion.GetIndent(1));
    }
}
void EndNamespace(string namespaceName)
{
    if (!String.IsNullOrEmpty(namespaceName))
    {
        PopIndent();
#>
}
<#+
    }
}
#>

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActivityRepository.cs" company="">
//   
// </copyright>
// <summary>
// The Activity repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ILC.Data.Repositories
{
    using ILC.Core.Data.Infrastructure;

    /// <summary>
    /// The ActivityRepository interface.
    /// </summary>
    public interface IActivityRepository : IRepository<Activity>
    {
    }
    
    /// <summary>
    /// The Activity repository.
    /// </summary>
    public partial class ActivityRepository : RepositoryBase<ILCDataContext, Activity>, IActivityRepository
    {    
        #region Constructors and Destructors
    
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityRepository"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        public ActivityRepository(IUnitOfWork<ILCDataContext> unitOfWork) 
            : base(unitOfWork.DataContext)
        {
        }    
    
        #endregion
    }
}

