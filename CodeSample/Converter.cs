﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeSample
{
    class Class2
    {
     /*Decimal converter*/
      decimal Debitvalue = 1156.547m;
      decimal DEBITAMT = Convert.ToDecimal(string.Format("{0:F2}", Debitvalue));
      var abc = string.Format(System.Globalization.CultureInfo.GetCultureInfo("de-EN"), "{0:0,0.###}", 22424242412.1223232323); 
    }
    
      public static DataTable ToDataTable(Stream fileStream, string[] columns, int startRowIndex, int startColumnIndex)
        {
            using XLWorkbook wb = new XLWorkbook(fileStream);
            var workSheet = wb.Worksheets.First();
            var range = workSheet.RangeUsed();
            var usedRange = range.Range(
                            startRowIndex,
                            startColumnIndex,
                            range.RangeAddress.LastAddress.RowNumber,
                            range.RangeAddress.LastAddress.ColumnNumber);
            var dataTable = new DataTable();
            for (var index = 0; index < columns.Length; index++)
            {
                dataTable.Columns.Add(columns[index]);
            }
            for (int rowIndex = startRowIndex; rowIndex < usedRange.RowCount(); rowIndex++)
            {
                var row = workSheet.Row(rowIndex);
                var newRow = dataTable.NewRow();
                foreach (var column in columns)
                {
                    var columnIndex = Array.IndexOf(columns, column) + 1;
                    var cell = row.Cell(Array.IndexOf(columns, column));
                    if (cell.DataType == XLDataType.DateTime)
                    {
                        newRow[column] = DateTime.FromOADate(double.Parse(cell.Value.ToString())).ToString(Consts.DATE_FORMAT);
                    }
                    else
                    {
                        newRow[column] = GetExcelCellValue(rowIndex, columnIndex, workSheet);
                    }
                }
                dataTable.Rows.Add(newRow);
            }

            return dataTable;

        }
}
