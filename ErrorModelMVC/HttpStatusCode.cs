namespace System.Net
{
  public enum HttpStatusCode
  {
    Continue = 100,
    SwitchingProtocols = 101,
    OK = 200,
    Created = 201,
    Accepted = 202,
    NonAuthoritativeInformation = 203,
    NoContent = 204,
    ResetContent = 205,
    PartialContent = 206,
    Ambiguous = 300,
    MultipleChoices = 300,
    Moved = 301,
    MovedPermanently = 301,
    Found = 302,
    Redirect = 302,
    RedirectMethod = 303,
    SeeOther = 303,
    NotModified = 304,
    UseProxy = 305,
    Unused = 306,
    RedirectKeepVerb = 307,
    TemporaryRedirect = 307,
    BadRequest = 400,
    Unauthorized = 401,
    PaymentRequired = 402,
    Forbidden = 403,
    NotFound = 404,
    MethodNotAllowed = 405,
    NotAcceptable = 406,
    ProxyAuthenticationRequired = 407,
    RequestTimeout = 408,
    Conflict = 409,
    Gone = 410,
    LengthRequired = 411,
    PreconditionFailed = 412,
    RequestEntityTooLarge = 413,
    RequestUriTooLong = 414,
    UnsupportedMediaType = 415,
    RequestedRangeNotSatisfiable = 416,
    ExpectationFailed = 417,
    InternalServerError = 500,
    NotImplemented = 501,
    BadGateway = 502,
    ServiceUnavailable = 503,
    GatewayTimeout = 504,
    HttpVersionNotSupported = 505,
  }
  /*
      <customErrors mode="On" redirectMode="ResponseRewrite">
      <error statusCode="404" redirect="~/Content/Errors/page404.aspx" />
      <error statusCode="500" redirect="~/Content/Errors/page500.aspx" />
    </customErrors>
  */
}

namespace OSMA.Security
{
    using System;

    public class Principal
    {
        public string AuthenticationType { get; set; }

        public PrincipalFunction CurrentFunction { get; set; }

        public PrincipalRole CurrentRole { get; set; }

        public string DisplayName { get; set; }

        public string Email { get; set; }

        public PrincipalFunction[] Functions { get; set; }

        public string ImagePath { get; set; }

        public string NTID { get; set; }

        public PrincipalPermission[] Permissions { get; set; }

        public PrincipalRole[] Roles { get; set; }

        public string SesssionId { get; set; }

        public object State { get; set; }
      
        public Guid UserId { get; set; }
      
        public bool IsRequireToChangePassword { get; set; }
     
        public System.Collections.Generic.IList<ApplicationLicense> Licenses { get; set; }
      
        public string WarningMessage { get; set; }

    }
}

namespace OSMA.Security
{
    using OSMA.Core.Infrastructure.EngineContext;
    using ILC.Core.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Security;

    /// <summary>
    /// The web security.
    /// </summary>
    public class WebSecurity : IWebSecurity
    {    
        public const string APP_AUTHENTICATION = "APP_ALL_USERS";
   
        private static List<Principal> applicationUsers;
       
        private Principal currentUser;
       
        public WebSecurity(HttpContextBase httpContext)
        {
            this.httpContext = httpContext;
        }
       
        public List<Principal> ApplicationUsers
        {
            get
            {
                if (this.httpContext == null)
                {
                    return new List<Principal>();
                }

                applicationUsers = (List<Principal>)this.httpContext.Application[APP_AUTHENTICATION];
                if (applicationUsers == null)
                {
                    applicationUsers = new List<Principal>();
                    this.httpContext.Application[APP_AUTHENTICATION] = applicationUsers;
                }

                return applicationUsers;
            }

            set
            {
                this.httpContext.Application[APP_AUTHENTICATION] = value;
            }
        }
       
        public Principal CurrentUser
        {
            get
            {
                HttpCookie authCookie = this.httpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (this.currentUser == null && authCookie != null)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    string userId = ticket.Name;
                    this.currentUser = this.ApplicationUsers.Where(p => p.UserId == Guid.Parse(userId)).FirstOrDefault();
                }
                return this.currentUser;
            }
        }
   
        public bool IsAuthenticated
        {
            get
            {
                return this.CurrentUser != null;
            }
        }
       
        private HttpContextBase httpContext { get; set; }

       
        public bool IsPermission(string permission)
        {
            return this.CurrentUser.Permissions.Any(c => c.PermissionId == permission);
        }
    
        public bool IsRole(Guid role)
        {
            return this.CurrentUser.Roles.Any(c => c.RoleId == role);
        }
      
        public bool IsRole(Guid roleId, Guid functionId)
        {
            foreach (var function in this.CurrentUser.Functions)
            {
                if (function.FunctionId == functionId)
                {
                    return function.Roles.Any(c => c == roleId);
                }
            }

            return false;
        }
     
        public void Login(Guid userId)
        {
            var webMembership = EngineContext.Current.Resolve<IWebMembership>();
            var user = webMembership.GetUserService(userId);
            this.Login(user);
        }
       
        public void Login(Principal user)
        {
            if (user != null)
            {
                var oldUserCache = this.ApplicationUsers.Where(p => p.UserId == user.UserId).ToList();
                foreach (var item in oldUserCache)
                {
                    this.ApplicationUsers.Remove(item);
                }

                FormsAuthentication.SignOut();

                user.AuthenticationType = this.httpContext.User.Identity.AuthenticationType != string.Empty
                                               ? this.httpContext.User.Identity.AuthenticationType
                                               : "Forms";

                // Save to Application cache
                this.ApplicationUsers.Add(user);

                var ticket = new FormsAuthenticationTicket(
                    1,
                    user.UserId.ToString(),
                    DateTime.Now,
                    DateTime.Now.AddMinutes(30),
                    false,
                    string.Empty,
                    FormsAuthentication.FormsCookiePath);

                var encryptedTicket = FormsAuthentication.Encrypt(ticket);

                this.httpContext.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                authCookie.Secure = false;
                //authCookie.HttpOnly = false;
                this.httpContext.Response.Cookies.Add(authCookie);

                var credentialLog = EngineContext.Current.Resolve<ILC.Services.Logging.IUserLoggingService>();
                credentialLog.WriteLog(user.UserId, "Log On");

                // Save to Session cache
                if (this.httpContext.Session != null)
                {
                    this.httpContext.Session[SS_AUTHENTICATION] = user;
                }
            }
        }

        public void Login(string token)
        {
            var webMembership = EngineContext.Current.Resolve<IWebMembership>();
            var user = webMembership.GetUserService(token);
            if (user != null)
            {
                var oldUserCache = this.ApplicationUsers.Where(p => p.UserId == user.UserId).ToList();
                foreach (var item in oldUserCache)
                {
                    this.ApplicationUsers.Remove(item);
                }

                //FormsAuthentication.SignOut();

                user.AuthenticationType = this.httpContext.User.Identity.AuthenticationType != string.Empty
                                               ? this.httpContext.User.Identity.AuthenticationType
                                               : "Forms";

                // Save to Application cache
                this.ApplicationUsers.Add(user);

                var ticket = new FormsAuthenticationTicket(
                    1,
                    user.UserId.ToString(),
                    DateTime.Now,
                    DateTime.Now.AddMinutes(30),
                    false,
                    string.Empty,
                    FormsAuthentication.FormsCookiePath);

                var encryptedTicket = FormsAuthentication.Encrypt(ticket);

                this.httpContext.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                authCookie.Secure = false;
                this.httpContext.Response.Cookies.Add(authCookie);

                // Save to Session cache
                if (this.httpContext.Session != null)
                {
                    this.httpContext.Session[SS_AUTHENTICATION] = user;
                }
            }
        }

        public void Logout()
        {
            if (this.IsAuthenticated)
            {
                var currentUserId = this.CurrentUser.UserId;

                // remove menu of current user
                if (this.httpContext.Application[Constant.APP_MENU_DISPLAY + currentUserId] != null)
                {
                    this.httpContext.Application[Constant.APP_MENU_DISPLAY + currentUserId] = null;
                }

                // remove current user on application 
                List<Principal> currentUsers = this.ApplicationUsers.Where(p => p.UserId == currentUserId).ToList();
                foreach (Principal item in currentUsers)
                {
                    this.ApplicationUsers.Remove(item);
                }

                // remove cookie value
                FormsAuthentication.SignOut();
                this.httpContext.Session.Abandon();

                // clear authentication cookie
                var cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty);
                cookie1.Expires = DateTime.Now.AddYears(-1);
                this.httpContext.Response.Cookies.Add(cookie1);

                // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
                HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", string.Empty);
                cookie2.Expires = DateTime.Now.AddYears(-1);
                this.httpContext.Response.Cookies.Add(cookie2);

                var credentialLog = EngineContext.Current.Resolve<ILC.Services.Logging.IUserLoggingService>();
                credentialLog.WriteLog(currentUserId, "Log Off");
            }
        }

        #endregion

        public bool ChangeFunction(Guid functionId)
        {
            if (!IsAuthenticated)
            {
                return false;
            }
            var webMembership = EngineContext.Current.Resolve<IWebMembership>();
            object state = CurrentUser.State;
            lock (state)
            {
                var function = CurrentUser.Functions.FirstOrDefault(c => c.FunctionId == functionId);
                CurrentUser.CurrentFunction = function;

                if (function != null)
                {
                    CurrentUser.Permissions = webMembership.GetPermissions(CurrentUser).ToArray();

                    webMembership.SetIsDefaultFunction(CurrentUser.UserId, functionId);
                }
            }
            // remove menu of current user
            if (this.httpContext.Application[Constant.APP_MENU_DISPLAY + this.CurrentUser.UserId] != null)
            {
                this.httpContext.Application[Constant.APP_MENU_DISPLAY + this.CurrentUser.UserId] = null;
            }
            return true;
        }

        public IEnumerable<PrincipalPermission> GetPermissions(string featureName)
        {
            return CurrentUser.Permissions.Where(
                c => c.FeatureName.ToLowerInvariant() == featureName.ToLowerInvariant());
        }
    }
}

//test